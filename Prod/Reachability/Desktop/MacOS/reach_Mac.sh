#!/bin/bash

echo "Waluv Reach Script for Mac OS X has started results will print momentarily..."
TIME1=$(date +%H:%M:%S)
time python3 reach_MojaveSafari.py
time python3 reach_MojaveFirefox.py
time python3 reach_MojaveChrome.py
time python3 reach_HSSafari.py
time python3 reach_HSFirefox.py
time python3 reach_HSChrome.py
time python3 reach_SierraSafari.py
time python3 reach_SierraFirefox.py
time python3 reach_SierraChrome.py
time python3 reach_ECSafari.py
time python3 reach_ECFirefox.py
time python3 reach_ECChrome.py
TIME2=$(date +%H:%M:%S)
echo "----------MAC-------------"
echo "Start of Script - $TIME1"
echo "End of Script - $TIME2"
echo "--------------------------"
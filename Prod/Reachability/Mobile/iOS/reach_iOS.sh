#!/bin/bash

echo "Waluv Reach Script for Apple iOS Mobile has started results will print momentarily..."
TIME1=$(date +%H:%M:%S)
time python3 iXs.py
time python3 iXsMax.py
time python3 iXR.py
time python3 iX.py
time python3 i8_12.py
time python3 i8_11.py
time python3 i8Plus.py
time python3 i7.py
time python3 i7Plus.py
time python3 i6s.py
time python3 i6sPlus.py
time python3 i6.py
time python3 iSE.py
TIME2=$(date +%H:%M:%S)
echo "-----------iOS-----------"
echo "Start of Script - $TIME1"
echo "End of Script - $TIME2"
echo "--------------------------"
cd iPad/
./reach_iOS-iPad.sh
#!/bin/bash

echo "Waluv Reach Script for Android Mobile: Samsung has started results will print momentarily..."
TIME1=$(date +%H:%M:%S)
time python3 gal_Note9.py
time python3 gal_S9Plus.py
time python3 gal_S9.py
time python3 gal_Note8.py
time python3 gal_A8.py
time python3 gal_S8Plus.py
time python3 gal_S8.py
time python3 gal_S7.py
time python3 gal_S6.py
TIME2=$(date +%H:%M:%S)
echo "-----------Android: Samsung-----------"
echo "Start of Script - $TIME1"
echo "End of Script - $TIME2"
echo "--------------------------"
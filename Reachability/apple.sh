#!/bin/bash

echo "Waluv Reach Script for Apple Ecosystem has started results will print momentarily..."
echo ""
TIME1=$(date +%H:%M:%S)
cd Desktop/MacOS
./reach_Mac.sh
cd .. && cd ..
cd Mobile/iOS
./reach_iOS.sh
TIME2=$(date +%H:%M:%S)
echo "----------Apple-------------"
echo "Start of Script - $TIME1"
echo "End of Script - $TIME2"
echo "--------------------------"
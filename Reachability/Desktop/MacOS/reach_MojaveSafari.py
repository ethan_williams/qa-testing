import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

start_time = time.time()

desired_cap = {
 'browser': 'Safari',
 'os': 'OS X',
 'os_version' : 'Mojave',
 'resolution' : '1024x768',
 'project' : 'Waluv',
 'build' : '0.1',
 'name' : 'Reachability Tests',
 'browserstack.debug' : 'true'
}

driver = webdriver.Remote(
    command_executor='http://ethanwilliams7:j7kwXUteG7ArojcjxG3P@hub.browserstack.com:80/wd/hub',
    desired_capabilities=desired_cap)

driver.get("http://www.waluv.com")
time.sleep(3)
if not "waluv" in driver.title:
    raise Exception("Unable to load Waluv page!")
print("Mac OS X Mojave - Safari - Test 1 / 12")
#print(sessionId)
driver.quit()
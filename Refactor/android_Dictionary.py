#	Mobile Environemnts 	##	##	##	##	##	##
###################################################
##	##	##	##	##	##	##	##	##	##	##	##	##
#	Android Mobile Environemnts + Browsers 	##	##	##
##	##	##	##	##	##	##	##	##	##	##	##	##
desired_cap = [
				{
				 'browserName': 'android',
				 'device': 'Google Pixel',
				 'realMobile': 'true',
				 'os_version': '7.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel',
				 'realMobile': 'true',
				 'os_version': '8.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel 2',
				 'realMobile': 'true',
				 'os_version': '8.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel 2',
				 'realMobile': 'true',
				 'os_version': '9.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel 3',
				 'realMobile': 'true',
				 'os_version': '9.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel 3 XL',
				 'realMobile': 'true',
				 'os_version': '9.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Nexus 5',
				 'realMobile': 'true',
				 'os_version': '4.4',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Nexus 6',
				 'realMobile': 'true',
				 'os_version': '5.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Nexus 6',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Nexus 9',
				 'realMobile': 'true',
				 'os_version': '5.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Tab S3',
				 'realMobile': 'true',
				 'os_version': '7.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Tab S4',
				 'realMobile': 'true',
				 'os_version': '8.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Tab 4',
				 'realMobile': 'true',
				 'os_version': '4.4',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				'browserName': 'android',
				 'device': 'Samsung Galaxy Note 4',
				 'realMobile': 'true',
				 'os_version': '4.4',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Note 4',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S6',
				 'realMobile': 'true',
				 'os_version': '5.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S7',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Note 4',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S8',
				 'realMobile': 'true',
				 'os_version': '7.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S8 Plus',
				 'realMobile': 'true',
				 'os_version': '7.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Note 8',
				 'realMobile': 'true',
				 'os_version': '7.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy A8',
				 'realMobile': 'true',
				 'os_version': '7.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S9',
				 'realMobile': 'true',
				 'os_version': '8.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S9 Plus',
				 'realMobile': 'true',
				 'os_version': '8.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Note 9',
				 'realMobile': 'true',
				 'os_version': '8.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Motorola Moto X 2nd Gen',
				 'realMobile': 'true',
				 'os_version': '5.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Motorola Moto X 2nd Gen',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				}
			  ]
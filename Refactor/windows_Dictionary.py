#	Desktop Environemnts 	##	##	##	##	##	##
###################################################
##	##	##	##	##	##	##	##	##	##	##	##	##
#	Windows Desktop Environemnts + Browsers 	##
##	##	##	##	##	##	##	##	##	##	##	##	##
###################################################
desired_cap = [
				{
				 'browser': 'IE',
				 'os': 'Windows',
				 'os_version' : '7',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'Windows',
				 'os_version' : '7',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'Windows',
				 'os_version' : '7',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'IE',
				 'os': 'Windows',
				 'os_version' : '8',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'Windows',
				 'os_version' : '8',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'Windows',
				 'os_version' : '8',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'IE',
				 'os': 'Windows',
				 'os_version' : '8.1',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'Windows',
				 'os_version' : '8.1',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'Windows',
				 'os_version' : '8.1',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'IE',
				 'os': 'Windows',
				 'os_version' : '10',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Edge',
				 'os': 'Windows',
				 'os_version' : '10',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'Windows',
				 'os_version' : '10',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'Windows',
				 'os_version' : '10',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				}
			   ]
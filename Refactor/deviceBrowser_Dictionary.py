#	Desktop Environemnts 	##	##	##	##	##	##
###################################################
##	##	##	##	##	##	##	##	##	##	##	##	##
#	Windows Desktop Environemnts + Browsers 	##
##	##	##	##	##	##	##	##	##	##	##	##	##
###################################################
desired_cap = [
				{
				 'browser': 'IE',
				 'os': 'Windows',
				 'os_version' : '7',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'Windows',
				 'os_version' : '7',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'Windows',
				 'os_version' : '7',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'IE',
				 'os': 'Windows',
				 'os_version' : '8',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'Windows',
				 'os_version' : '8',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'Windows',
				 'os_version' : '8',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'IE',
				 'os': 'Windows',
				 'os_version' : '8.1',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'Windows',
				 'os_version' : '8.1',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'Windows',
				 'os_version' : '8.1',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'IE',
				 'os': 'Windows',
				 'os_version' : '10',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Edge',
				 'os': 'Windows',
				 'os_version' : '10',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'Windows',
				 'os_version' : '10',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'Windows',
				 'os_version' : '10',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
##	##	##	##	##	##	##	##	##	##	##	##	##
#	MacOS Desktop Environments + Browsers
##	##	##	##	##	##	##	##	##	##	##	##	##
				{
				 'browser': 'Safari',
				 'os': 'OS X',
				 'os_version' : 'El Capitan',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'OS X',
				 'os_version' : 'El Capitan',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'OS X',
				 'os_version' : 'El Capitan',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Safari',
				 'os': 'OS X',
				 'os_version' : 'Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'OS X',
				 'os_version' : 'Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				}
				 {
				 'browser': 'Chrome',
				 'os': 'OS X',
				 'os_version' : 'Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Safari',
				 'os': 'OS X',
				 'os_version' : 'High Sierra',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'OS X',
				 'os_version' : 'High Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'OS X',
				 'os_version' : 'High Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Safari',
				 'os': 'OS X',
				 'os_version' : 'Mojave',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'OS X',
				 'os_version' : 'Mojave',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'OS X',
				 'os_version' : 'Mojave',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
#	Mobile Environemnts 	##	##	##	##	##	##
###################################################
##	##	##	##	##	##	##	##	##	##	##	##	##
#	iOS Mobile Environemnts + Browsers 	##	##	##
##	##	##	##	##	##	##	##	##	##	##	##	##
###################################################
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 6',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 6S',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 6S Plus',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 7',
				 'realMobile': 'true',
				 'os_version': '10',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 7 Plus',
				 'realMobile': 'true',
				 'os_version': '10',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 8',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 8 Plus',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 8',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone SE',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone X',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone XS',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone XS Max',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone XR',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad 5th',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad 6th',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Mini 4',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Pro 9.7 2016',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Pro 11 2018',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Pro 12.9 2017',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Pro 12.9 2018',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
##	##	##	##	##	##	##	##	##	##	##	##	##
#	Android Mobile Environemnts + Browsers 	##	##	##
##	##	##	##	##	##	##	##	##	##	##	##	##
				{
				 'browserName': 'android',
				 'device': 'Google Pixel',
				 'realMobile': 'true',
				 'os_version': '7.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel',
				 'realMobile': 'true',
				 'os_version': '8.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel 2',
				 'realMobile': 'true',
				 'os_version': '8.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel 2',
				 'realMobile': 'true',
				 'os_version': '9.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel 3',
				 'realMobile': 'true',
				 'os_version': '9.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Pixel 3 XL',
				 'realMobile': 'true',
				 'os_version': '9.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Nexus 5',
				 'realMobile': 'true',
				 'os_version': '4.4',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Nexus 6',
				 'realMobile': 'true',
				 'os_version': '5.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Nexus 6',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Google Nexus 9',
				 'realMobile': 'true',
				 'os_version': '5.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Tab S3',
				 'realMobile': 'true',
				 'os_version': '7.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Tab S4',
				 'realMobile': 'true',
				 'os_version': '8.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Tab 4',
				 'realMobile': 'true',
				 'os_version': '4.4',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				'browserName': 'android',
				 'device': 'Samsung Galaxy Note 4',
				 'realMobile': 'true',
				 'os_version': '4.4',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Note 4',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S6',
				 'realMobile': 'true',
				 'os_version': '5.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S7',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Note 4',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S8',
				 'realMobile': 'true',
				 'os_version': '7.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S8 Plus',
				 'realMobile': 'true',
				 'os_version': '7.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Note 8',
				 'realMobile': 'true',
				 'os_version': '7.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy A8',
				 'realMobile': 'true',
				 'os_version': '7.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S9',
				 'realMobile': 'true',
				 'os_version': '8.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy S9 Plus',
				 'realMobile': 'true',
				 'os_version': '8.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Samsung Galaxy Note 9',
				 'realMobile': 'true',
				 'os_version': '8.1',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Motorola Moto X 2nd Gen',
				 'realMobile': 'true',
				 'os_version': '5.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'android',
				 'device': 'Motorola Moto X 2nd Gen',
				 'realMobile': 'true',
				 'os_version': '6.0',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				}
			  ]
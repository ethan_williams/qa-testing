#	Desktop Environemnts 	##	##	##	##	##	##
###################################################
##	##	##	##	##	##	##	##	##	##	##	##	##
#	MacOS Desktop Environments + Browsers
##	##	##	##	##	##	##	##	##	##	##	##	##
desired_cap = [	
				{
				 'browser': 'Safari',
				 'os': 'OS X',
				 'os_version' : 'El Capitan',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'OS X',
				 'os_version' : 'El Capitan',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'OS X',
				 'os_version' : 'El Capitan',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Safari',
				 'os': 'OS X',
				 'os_version' : 'Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'OS X',
				 'os_version' : 'Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				}
				 {
				 'browser': 'Chrome',
				 'os': 'OS X',
				 'os_version' : 'Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Safari',
				 'os': 'OS X',
				 'os_version' : 'High Sierra',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'OS X',
				 'os_version' : 'High Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'OS X',
				 'os_version' : 'High Sierra',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Safari',
				 'os': 'OS X',
				 'os_version' : 'Mojave',
				 'resolution' : '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Firefox',
				 'os': 'OS X',
				 'os_version' : 'Mojave',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browser': 'Chrome',
				 'os': 'OS X',
				 'os_version' : 'Mojave',
				 'resolution': '1024x768',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				}
			  ]
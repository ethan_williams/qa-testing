#	Mobile Environemnts 	##	##	##	##	##	##
###################################################
##	##	##	##	##	##	##	##	##	##	##	##	##
#	iOS Mobile Environemnts + Browsers 	##	##	##
##	##	##	##	##	##	##	##	##	##	##	##	##
###################################################
desired_cap = [
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 6',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 6S',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 6S Plus',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 7',
				 'realMobile': 'true',
				 'os_version': '10',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 7 Plus',
				 'realMobile': 'true',
				 'os_version': '10',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 8',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 8 Plus',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone 8',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone SE',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone X',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone XS',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone XS Max',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPhone',
				 'device': 'iPhone XR',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad 5th',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad 6th',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Mini 4',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Pro 9.7 2016',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Pro 11 2018',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Pro 12.9 2017',
				 'realMobile': 'true',
				 'os_version': '11',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				},
				{
				 'browserName': 'iPad',
				 'device': 'iPad Pro 12.9 2018',
				 'realMobile': 'true',
				 'os_version': '12',
				 'project' : 'Waluv',
				 'build' : '0.1',
				 'name' : 'Reachability Tests',
				 'browserstack.debug' : 'true'
				}
			  ]
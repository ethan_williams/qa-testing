#!/bin/bash

echo "Waluv Reach Script for Windows has started results will print momentarily..."
TIME1=$(date +%H:%M:%S)
time python3 reach_Win10IE.py
time python3 reach_Win10Edge.py
time python3 reach_Win10Firefox.py
time python3 reach_Win10Chrome.py
time python3 reach_Win8IE.py
time python3 reach_Win8Firefox.py
time python3 reach_Win8Chrome.py
time python3 reach_Win8_1IE.py
time python3 reach_Win8_1Firefox.py
time python3 reach_Win8_1Chrome.py
time python3 reach_Win7IE.py
time python3 reach_Win7Firefox.py
time python3 reach_Win7Chrome.py
TIME2=$(date +%H:%M:%S)
echo "--------------------------"
echo "Start of Script - $TIME1"
echo "End of Script - $TIME2"
echo "--------------------------"
#!/bin/bash

TIME1=$(date +%H:%M:%S)
echo "Waluv Reach Script for Desktop has started: "
echo "" 
cd MacOS/
./reach_Mac.sh
cd ..
cd Windows/
./reach_Windows.sh
TIME2=$(date +%H:%M:%S)
echo "--------------------------"
echo "Desktop Test Reach"
echo "-- -- -- -- -- -- -- -- --"
echo "Start of Script - $TIME1"
echo "End of Script - $TIME2"
echo "--------------------------"
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

desired_cap = {
 'browserName': 'android',
 'device': 'Samsung Galaxy Note 9',
 'realMobile': 'true',
 'os_version': '8.1',
 'project' : 'Waluv',
 'build' : '0.1',
 'name' : 'Reachability Tests',
 'browserstack.debug' : 'true'
}

driver = webdriver.Remote(
    command_executor='http://ethanwilliams7:j7kwXUteG7ArojcjxG3P@hub.browserstack.com:80/wd/hub',
    desired_capabilities=desired_cap)

driver.get("http://dev.waluv.com")
time.sleep(3)
if not "waluv" in driver.title:
    raise Exception("Unable to load Waluv page!")
print("Android - Samsung - Chrome - Test 1 / 9")
#print(sessionId)
driver.quit()
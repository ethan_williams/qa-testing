#!/bin/bash

echo "Waluv Reach Script for Apple iOS iPad has started results will print momentarily..."
TIME1=$(date +%H:%M:%S)
time python3 iPad12_2018.py
time python3 iPad11_2018.py
time python3 iPad9_2016.py
time python3 iPad12_2017.py
time python3 iPad_Mini4.py
time python3 iPad_6th.py
time python3 iPad_5th.py
TIME2=$(date +%H:%M:%S)
echo "-----------iPad------------"
echo "Start of Script - $TIME1"
echo "End of Script - $TIME2"
echo "--------------------------"
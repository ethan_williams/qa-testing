import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

desired_cap = {
 'browserName': 'iPad',
 'device': 'iPad Pro 12.9 2018',
 'realMobile': 'true',
 'os_version': '12',
 'project' : 'Waluv',
 'build' : '0.1',
 'name' : 'Reachability Tests',
 'browserstack.debug' : 'true'
}

driver = webdriver.Remote(
    command_executor='http://ethanwilliams7:j7kwXUteG7ArojcjxG3P@hub.browserstack.com:80/wd/hub',
    desired_capabilities=desired_cap)

driver.get("http://dev.waluv.com")
time.sleep(3)
if not "waluv" in driver.title:
    raise Exception("Unable to load Waluv page!")
print("Apple iOS:iPad - Safari - Test 1 / 13")
#print(sessionId)
driver.quit()
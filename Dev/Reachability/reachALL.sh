#!/bin/bash

cd Mobile/
TIME1=$(date +%H:%M:%S)
./reach_Mobile.sh
TIME2=$(date +%H:%M:%S)
cd .. && cd Desktop/
TIME3=$(date +%H:%M:%S)
./reach_Desktop.sh
TIME4=$(date +%H:%M:%S)
clear
echo "--------------------------"
echo "Mobile Test Reach"
echo "-- -- -- -- -- -- -- -- --"
echo "Start of Script - $TIME1"
echo "End of Script - $TIME2"
echo "--------------------------"
echo ""
echo "--------------------------"
echo "Desktop Test Reach"
echo "-- -- -- -- -- -- -- -- --"
echo "Start of Script - $TIME3"
echo "End of Script - $TIME4"
echo "--------------------------"
echo ""
echo "--------------------------"
echo "Total Test Reach"
echo "-- -- -- -- -- -- -- -- --"
echo "Start of Script - $TIME1"
echo "End of Script - $TIME4"
echo "--------------------------"